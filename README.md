# MoneyTransfer

Bank accounts example applications
Features : 
  - Create account 
  - Query account by id
  - Transfer money between accounts

### Installation
MoneyTransfer application uses Maven. To build executable jar please use 

```sh
$ mvn package
```

### Configuration
MoneyTransfer uses properties.
For example if you want to change server.port you can do it by changes in file application.properties.

### REST API
| Request Type | Request | Description |
| ------ | ------ | ------ |
| POST | /account?amount=${amount}}&id=${id} | Create new account with given id and amount of money |
| GET | /account?amount=id=${id} | Query account with given id and return json with amount of money |
| POST | /transfer?amount=${amount}&fromId={fromId}&toId=${toId} | Transfer given amount of money from one account to another |

#### E2E Test
E2E test to present funcionality of this application is created. Please see 
```sh
org.luk.transfer.server.ServerE2ETest.java
```
