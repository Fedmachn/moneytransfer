package org.luk.transfer.server;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.luk.transfer.factory.DefaultMoneyTransferFactory;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;

import static org.junit.Assert.assertEquals;

public class ServerE2ETest {

    private static final String PROTOCOL = "http";
    private static Server server;
    private static int port;
    private static String host;

    @BeforeClass
    public static void startServer() {
        ServerProperties serverProperties = new ServerFileProperties();
        server = new Server(serverProperties, new DefaultMoneyTransferFactory());
        port = serverProperties.getPort();
        host = serverProperties.getHost();
        server.start();
    }

    @AfterClass
    public static void closeHttpClient() {
        if (server != null) {
            server.stop();
        }
    }

    @Test
    public void addAccount() throws URISyntaxException, IOException {
//        Given
        int id = 1;
        HttpPost createAccountRequest = getHttpCreateAccountRequest(id, BigDecimal.valueOf(12));

//        When
        CloseableHttpResponse executeResult = HttpClientBuilder.create().build().execute(createAccountRequest);

//        Then
        assertEquals(201, executeResult.getStatusLine().getStatusCode());
        executeResult.close();
    }

    @Test
    public void addAndQueryAccount() throws URISyntaxException, IOException {
//        Given
        int id = 2;
        HttpGet queryAccountRequest = getHttpQueryAccountRequest(id);
        HttpPost createAccountRequest = getHttpCreateAccountRequest(2, BigDecimal.valueOf(12));
        CloseableHttpClient client = HttpClientBuilder.create().build();
        client.execute(createAccountRequest).close();

//        When
        CloseableHttpResponse executeResult = client.execute(queryAccountRequest);

//        Then
        assertEquals(200, executeResult.getStatusLine().getStatusCode());
        executeResult.close();
    }

    @Test
    public void addDuplicatedAccount() throws URISyntaxException, IOException {
//        Given
        int firstId = 3;
        HttpPost createAccountRequest = getHttpCreateAccountRequest(firstId, BigDecimal.valueOf(12));
        HttpPost createAccountRequest2 = getHttpCreateAccountRequest(firstId, BigDecimal.valueOf(12));
        CloseableHttpClient client = HttpClientBuilder.create().build();

//        When
        CloseableHttpResponse executeResult = client.execute(createAccountRequest);
        CloseableHttpResponse executeResult2 = client.execute(createAccountRequest2);

//        Then
        assertEquals(201, executeResult.getStatusLine().getStatusCode());
        assertEquals(400, executeResult2.getStatusLine().getStatusCode());
        executeResult.close();
        executeResult2.close();
    }

    @Test
    public void queryNotCreatedAccount() throws URISyntaxException, IOException {
//        Given
        int id = 4;
        HttpGet queryAccountRequest = getHttpQueryAccountRequest(id);
        CloseableHttpClient client = HttpClientBuilder.create().build();

//        When
        CloseableHttpResponse executeResult = client.execute(queryAccountRequest);

//        Then
        assertEquals(404, executeResult.getStatusLine().getStatusCode());
        executeResult.close();
    }

    @Test
    public void transferBetweenAccountsTest() throws URISyntaxException, IOException {
//        Given
        int firstId = 5;
        int secondId = 6;
        HttpPost createAccountRequest = getHttpCreateAccountRequest(firstId, BigDecimal.valueOf(12));
        HttpPost createAccountRequest2 = getHttpCreateAccountRequest(secondId, BigDecimal.valueOf(12));
        HttpPost createAccountRequestTransfer = getHttpTransferRequest(BigDecimal.valueOf(12), firstId, secondId);
        CloseableHttpClient client = HttpClientBuilder.create().build();

//        When
        client.execute(createAccountRequest).close();
        client.execute(createAccountRequest2).close();

        CloseableHttpResponse executeResultTransfer = client.execute(createAccountRequestTransfer);
        int transferStatusCode = executeResultTransfer.getStatusLine().getStatusCode();
        executeResultTransfer.close();

        BigDecimal amountFirst = getAmountFromAccount(firstId, client);
        BigDecimal amountSecond = getAmountFromAccount(secondId, client);

//        Then
        assertEquals(200, transferStatusCode);
        assertEquals(0, BigDecimal.ZERO.compareTo(amountFirst));
        assertEquals(0, BigDecimal.valueOf(24).compareTo(amountSecond));
    }

    @Test
    public void transferBetweenAccountsNotEnoughMoneyTest() throws URISyntaxException, IOException {
//        Given
        int firstId = 7;
        int secondId = 8;
        HttpPost createAccountRequest = getHttpCreateAccountRequest(firstId, BigDecimal.valueOf(12));
        HttpPost createAccountRequest2 = getHttpCreateAccountRequest(secondId, BigDecimal.valueOf(12));
        HttpPost createAccountRequestTransfer = getHttpTransferRequest(BigDecimal.valueOf(15), firstId, secondId);
        CloseableHttpClient client = HttpClientBuilder.create().build();

//        When
        client.execute(createAccountRequest).close();
        client.execute(createAccountRequest2).close();

        CloseableHttpResponse executeResultTransfer = client.execute(createAccountRequestTransfer);
        int transferStatusCode = executeResultTransfer.getStatusLine().getStatusCode();
        executeResultTransfer.close();

        BigDecimal amountFirst = getAmountFromAccount(firstId, client);
        BigDecimal amountSecond = getAmountFromAccount(secondId, client);

//        Then
        assertEquals(400, transferStatusCode);
        assertEquals(0, BigDecimal.valueOf(12).compareTo(amountFirst));
        assertEquals(0, BigDecimal.valueOf(12).compareTo(amountSecond));
    }

    private BigDecimal getAmountFromAccount(int firstId, CloseableHttpClient client) throws URISyntaxException, IOException {
        HttpGet queryAccountRequest = getHttpQueryAccountRequest(firstId);
        CloseableHttpResponse queryResult = client.execute(queryAccountRequest);
        Gson gson = new Gson();
        Type type = new TypeToken<HashMap<String, String>>(){}.getType();
        HashMap<String, String> result = gson.fromJson(new InputStreamReader(queryResult.getEntity().getContent()), type);
        queryResult.close();
        return new BigDecimal(result.get("value"));
    }

    private HttpPost getHttpCreateAccountRequest(int id, BigDecimal amount) throws URISyntaxException {
        URI createAccountUri = new URIBuilder()
                .setScheme(PROTOCOL)
                .setHost(host)
                .setPort(port)
                .setPath("account")
                .setParameter("id", String.valueOf(id))
                .setParameter("amount", String.valueOf(amount))
                .build();
        return new HttpPost(createAccountUri);
    }

    private HttpGet getHttpQueryAccountRequest(int id) throws URISyntaxException {
        URI createAccountUri = new URIBuilder()
                .setScheme(PROTOCOL)
                .setHost(host)
                .setPort(port)
                .setPath("account")
                .setParameter("id", String.valueOf(id))
                .build();
        return new HttpGet(createAccountUri);
    }

    private HttpPost getHttpTransferRequest(BigDecimal amount, int fromAccount, int toAccount) throws URISyntaxException {
        URI createAccountUri = new URIBuilder()
                .setScheme(PROTOCOL)
                .setHost(host)
                .setPort(port)
                .setPath("transfer")
                .setParameter("fromId", String.valueOf(fromAccount))
                .setParameter("toId", String.valueOf(toAccount))
                .setParameter("amount", String.valueOf(amount))
                .build();
        return new HttpPost(createAccountUri);
    }

}