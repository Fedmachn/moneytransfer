package org.luk.transfer.account;

import org.luk.transfer.exceptions.NotEnoughMoneyTransferException;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;

public class DefaultAccountTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultAccountTest.class);

    @Test
    public void transferWithSuccessTest() {
//        Given
        DefaultAccount first = new DefaultAccount(1, BigDecimal.valueOf(12));
        DefaultAccount second = new DefaultAccount(2, BigDecimal.valueOf(10));

//        When
        first.transferTo(BigDecimal.valueOf(12), second);

//        Then
        assertEquals(BigDecimal.ZERO, first.getValue());
        assertEquals(BigDecimal.valueOf(22), second.getValue());
    }

    @Test(expected = NotEnoughMoneyTransferException.class)
    public void toLittleMoneyToTransferTest() {
//        Given
        DefaultAccount first = new DefaultAccount(1, BigDecimal.valueOf(12));
        DefaultAccount second = new DefaultAccount(2, BigDecimal.valueOf(10));

//        When
        first.transferTo(BigDecimal.valueOf(13), second);

    }

    @Test
    public void manyThreadTest() throws InterruptedException {
//        Given
        DefaultAccount first = new DefaultAccount(1, BigDecimal.valueOf(10));
        DefaultAccount second = new DefaultAccount(2, BigDecimal.valueOf(10));

//        When
        ExecutorService executorService = Executors.newFixedThreadPool(10);
        final CyclicBarrier gate = new CyclicBarrier(10);
        for (int i = 0; i < 5; i++) {
            executorService.submit(() -> {
                try {
                    gate.await();
                } catch (Exception e) {
                    LOGGER.error("Error during concurrent test", e);
                }
                first.transferTo(BigDecimal.valueOf(1), second);
            });
            executorService.submit(() -> {
                try {
                    gate.await();
                } catch (Exception e) {
                    LOGGER.error("Error during concurrent test", e);
                }
                second.transferTo(BigDecimal.valueOf(2), first);
            });
        }
        executorService.awaitTermination(1, TimeUnit.SECONDS);

//        Then
        assertEquals(BigDecimal.valueOf(15), first.getValue());
        assertEquals(BigDecimal.valueOf(5), second.getValue());
    }
}