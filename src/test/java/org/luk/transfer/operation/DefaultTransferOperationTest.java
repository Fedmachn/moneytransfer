package org.luk.transfer.operation;

import org.luk.transfer.account.Account;
import org.luk.transfer.account.AccountRepository;
import org.luk.transfer.account.DefaultAccount;
import org.luk.transfer.account.InMemoryAccountRepository;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Optional;

import static org.junit.Assert.*;

public class DefaultTransferOperationTest {

    @Test
    public void transferTest() {
//        Given
        AccountRepository repo = new InMemoryAccountRepository();
        DefaultTransferOperation op = new DefaultTransferOperation(repo);
        BigDecimal amount = BigDecimal.valueOf(12);
        int fromId = 1;
        int toId = 2;
        Account first = new DefaultAccount(fromId, amount);
        Account second = new DefaultAccount(toId, amount);

//        When
        repo.save(first);
        repo.save(second);
        op.transfer(amount, fromId, toId);

//        Then
        Optional<Account> firstResult = repo.findById(fromId);
        Optional<Account> secondResult = repo.findById(toId);

        assertTrue(firstResult.isPresent());
        assertTrue(secondResult.isPresent());
        assertEquals(BigDecimal.ZERO, firstResult.get().getValue());
        assertEquals(BigDecimal.valueOf(24), secondResult.get().getValue());

    }

}