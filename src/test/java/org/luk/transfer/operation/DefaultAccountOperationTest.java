package org.luk.transfer.operation;

import org.luk.transfer.account.Account;
import org.luk.transfer.account.AccountRepository;
import org.luk.transfer.account.InMemoryAccountRepository;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Optional;

import static org.junit.Assert.*;

public class DefaultAccountOperationTest {

    @Test
    public void accountTest() {
//        Given
        AccountRepository repo = new InMemoryAccountRepository();
        DefaultAccountOperation op = new DefaultAccountOperation(repo);
        int id = 1;
        BigDecimal amount = BigDecimal.valueOf(12);

//        When
        op.addAccount(id, amount);

//        Then
        Optional<Account> result = op.getAccount(id);

        assertTrue(result.isPresent());
        assertEquals(amount, result.get().getValue());
    }

}