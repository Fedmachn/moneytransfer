package org.luk.transfer.exceptions;

public class TransferBeetwenAccountsEception extends RuntimeException {

    public TransferBeetwenAccountsEception(String message) {
        super(message);
    }
}
