package org.luk.transfer.exceptions;

public class RequestParameterException extends RuntimeException {

    public RequestParameterException(String message) {
        super(message);
    }
}
