package org.luk.transfer.exceptions;

public class UniqueAccountRepositoryEception extends RuntimeException {

    public UniqueAccountRepositoryEception(String message) {
        super(message);
    }
}
