package org.luk.transfer.operation;

import org.luk.transfer.account.Account;

import java.math.BigDecimal;
import java.util.Optional;

public interface AccountOperation {

    void addAccount(int id, BigDecimal amount);
    Optional<Account> getAccount(int id);
}
