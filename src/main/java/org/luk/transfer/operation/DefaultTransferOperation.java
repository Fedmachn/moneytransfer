package org.luk.transfer.operation;

import org.luk.transfer.account.Account;
import org.luk.transfer.account.AccountRepository;
import org.luk.transfer.exceptions.NoAccountException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;

public class DefaultTransferOperation implements TransferOperation {
    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultTransferOperation.class);
    private AccountRepository accountRepository;

    public DefaultTransferOperation(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    public void transfer(BigDecimal amount, int fromAccountId, int toAccountId) {
        LOGGER.debug("Money Transfer, amount:{} ; fromAccountId:{} ; toAccountId:{}", amount, fromAccountId, toAccountId);

        Account fromAccount = accountRepository
                .findById(fromAccountId)
                .orElseThrow(() -> new NoAccountException("There isn't account with id " + fromAccountId));
        Account toAccount = accountRepository
                .findById(toAccountId)
                .orElseThrow(() -> new NoAccountException("There isn't account with id " + toAccountId));

        fromAccount.transferTo(amount, toAccount);
    }


}
