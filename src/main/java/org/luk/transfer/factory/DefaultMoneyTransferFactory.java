package org.luk.transfer.factory;

import org.luk.transfer.account.AccountRepository;
import org.luk.transfer.account.InMemoryAccountRepository;
import org.luk.transfer.operation.AccountOperation;
import org.luk.transfer.operation.DefaultAccountOperation;
import org.luk.transfer.operation.DefaultTransferOperation;
import org.luk.transfer.operation.TransferOperation;
import org.luk.transfer.server.DefaultHttpHandlerFactory;
import org.luk.transfer.server.HttpHandlerFactory;

public class DefaultMoneyTransferFactory implements MoneyTransferFactory{

    @Override
    public HttpHandlerFactory getHttpHandlerFactory() {
        AccountRepository accountRepository = getAccountRepository();
        return new DefaultHttpHandlerFactory(getTransferOperation(accountRepository), getAccountOperation(accountRepository));
    }

    @Override
    public AccountOperation getAccountOperation(AccountRepository accountRepository) {
        return new DefaultAccountOperation(accountRepository);
    }

    @Override
    public TransferOperation getTransferOperation(AccountRepository accountRepository) {
        return new DefaultTransferOperation(accountRepository);
    }

    @Override
    public AccountRepository getAccountRepository() {
        return new InMemoryAccountRepository();
    }

}
