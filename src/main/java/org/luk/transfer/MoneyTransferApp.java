package org.luk.transfer;

import org.luk.transfer.factory.DefaultMoneyTransferFactory;
import org.luk.transfer.server.Server;
import org.luk.transfer.server.ServerFileProperties;
import org.luk.transfer.server.ServerProperties;

public class MoneyTransferApp {

    public static void main(String[] args) {
        ServerProperties serverProperties = new ServerFileProperties();
        Server server = new Server(serverProperties, new DefaultMoneyTransferFactory());
        server.start();
    }

}
