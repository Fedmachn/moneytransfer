package org.luk.transfer.account;

import org.luk.transfer.exceptions.NotEnoughMoneyTransferException;
import org.luk.transfer.exceptions.TransferBeetwenAccountsEception;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;

import static java.lang.String.*;
import static java.math.BigDecimal.ZERO;

public class DefaultAccount implements Account {
    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultAccount.class);
    private final int id;
    private final AtomicReference<BigDecimal> value;


    public DefaultAccount(int id, BigDecimal value) {
        this.value = new AtomicReference<>(value);
        this.id = id;
    }

    @Override
    public BigDecimal getValue() {
        return value.get();
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public void transferTo(BigDecimal amount, Account to) {
        LOGGER.debug("DefaultAccount :{}, amount :{}", getId(), amount);

        BigDecimal original = null;
        String errorMessage = null;
        boolean notEnoughMoney = false;
        try {
            original = value.getAndAccumulate(amount, (originalValue, amountToTake) -> {
                BigDecimal result = originalValue.subtract(amountToTake);
                return result.compareTo(ZERO) >= 0 ? result : originalValue;
            });
        } finally {
            if (original == null) {
                errorMessage = "Unexpected problem during transfer";
            } else if (original.subtract(amount).compareTo(ZERO) < 0) {
                notEnoughMoney = true;
                errorMessage = format("On the account %d isn't enough resources to conduct transfer with value %s to account %d",
                                getId(), amount.toString(), to.getId());
            } else {
                to.acceptTransfer(amount);
            }
        }
        if (errorMessage != null) {
            throw notEnoughMoney ?
                    new NotEnoughMoneyTransferException(errorMessage) : new TransferBeetwenAccountsEception(errorMessage);
        }
        LOGGER.debug("Finished :{}, amount :{}", getId(), amount);
    }

    @Override
    public void acceptTransfer(BigDecimal amount) {
        value.getAndAccumulate(amount, BigDecimal::add);
    }

    @Override
    public String toString() {
        return "DefaultAccount{" +
                "id=" + id +
                ", value=" + value +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DefaultAccount defaultAccount = (DefaultAccount) o;
        return getId() == defaultAccount.getId() &&
                getValue().equals(defaultAccount.getValue());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getValue());
    }
}
