package org.luk.transfer.account;

import java.util.Optional;

public interface AccountRepository {

    Optional<Account> findById(int id);

    Account save(Account account);
}
