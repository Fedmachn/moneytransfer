package org.luk.transfer.account;

import java.math.BigDecimal;

public interface Account {
    BigDecimal getValue();

    int getId();

    void transferTo(BigDecimal amount, Account to);

    void acceptTransfer(BigDecimal amount);
}
