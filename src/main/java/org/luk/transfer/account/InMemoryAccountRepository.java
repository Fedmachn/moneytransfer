package org.luk.transfer.account;

import org.luk.transfer.exceptions.AccountRepositoryException;
import org.luk.transfer.exceptions.UniqueAccountRepositoryEception;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

public class InMemoryAccountRepository implements AccountRepository {

    private final Map<Integer, Account> inMemory = new ConcurrentHashMap<>();

    @Override
    public Optional<Account> findById(int id) {
        return Optional.ofNullable(inMemory.get(id));
    }

    @Override
    public Account save(Account account) {
        if (account != null) {
            if (inMemory.containsKey(account.getId())) {
                throw new UniqueAccountRepositoryEception("Unique constraint exception. Account with key : "
                        + account.getId() + " already exist in repository");
            }
            inMemory.put(account.getId(), account);
            return account;
        }
        throw new AccountRepositoryException("Invalid data to save. Given Account is null ");
    }

}
