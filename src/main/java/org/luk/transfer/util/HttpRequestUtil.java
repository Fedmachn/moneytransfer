package org.luk.transfer.util;

import org.luk.transfer.exceptions.RequestParameterException;

import java.math.BigDecimal;
import java.util.Deque;
import java.util.Map;
import java.util.NoSuchElementException;

public class HttpRequestUtil {

    static final String ID_PARAM = "id";
    static final String AMOUNT_PARAM = "amount";
    static final String FROM_ID_PARAM = "fromId";
    static final String TO_ID_PARAM = "toId";

    public static int getIdParam(Map<String, Deque<String>> params) {
        return getIDTypeParam(params, ID_PARAM);
    }

    public static int getFromAccountParam(Map<String, Deque<String>> params) {
        return getIDTypeParam(params, FROM_ID_PARAM);
    }

    public static int getToAccountParam(Map<String, Deque<String>> params) {
        return getIDTypeParam(params, TO_ID_PARAM);
    }

    private static int getIDTypeParam(Map<String, Deque<String>> params, String idParam) {
        try {
            return Integer.decode(params.get(idParam).getFirst());
        } catch (NoSuchElementException | NumberFormatException | NullPointerException e) {
            throw new RequestParameterException("Parameter " + idParam + " is mandatory and it should be integer value");
        }
    }

    public static BigDecimal getAmountParam(Map<String, Deque<String>> params) {
        try {
            return new BigDecimal(params.get(AMOUNT_PARAM).getFirst());
        } catch (NoSuchElementException | NumberFormatException | NullPointerException e) {
            throw new RequestParameterException("Parameter amount is mandatory and it should be BigDecimal value");
        }
    }
}
