package org.luk.transfer.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import static java.lang.String.format;

public class PropertiesUtil {
    private static final String PROPERTIES_PATH = "application.properties";
    private static Properties prop;


    static {

        try (InputStream inputStream = PropertiesUtil.class.getClassLoader().getResourceAsStream(PROPERTIES_PATH)) {
            if (inputStream == null) {
                throw new RuntimeException(
                        format("There are not configured %s file. It is mandatory for this application",
                                PROPERTIES_PATH));
            }
            prop = new Properties();
            prop.load(inputStream);
        } catch (IOException e) {
            throw new RuntimeException(format("There is a problem with reading %s file", PROPERTIES_PATH), e);
        }
    }

    public static String getProperty(String key) {
        return prop.getProperty(key);
    }

    public static String getOrDefaultProperty(String key, String defValue) {
        return prop.getProperty(key, defValue);
    }

    public static Object get(String key) {
        return prop.getProperty(key);
    }
}
