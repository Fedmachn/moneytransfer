package org.luk.transfer.server;

import org.luk.transfer.factory.MoneyTransferFactory;
import io.undertow.Handlers;
import io.undertow.Undertow;
import io.undertow.server.HttpHandler;
import io.undertow.server.RoutingHandler;

public class Server {
    private static final String TRANSFER_PATH = "/transfer";
    private static final String ACCOUNT_PATH = "/account";
    private final MoneyTransferFactory moneyServerFactory;
    private final ServerProperties serverProperties;

    private Undertow server;

    public Server(ServerProperties serverProperties, MoneyTransferFactory moneyTransferFactory) {
        if (serverProperties == null) {
            throw new IllegalArgumentException("ServerProperties cannot be null");
        }
        if (moneyTransferFactory == null) {
            throw new IllegalArgumentException("MoneyTransferFactory cannot be null");
        }
        this.serverProperties = serverProperties;
        this.moneyServerFactory = moneyTransferFactory;
    }

    public void start() {
        HttpHandlerFactory httpHandlerFactory = moneyServerFactory.getHttpHandlerFactory();

        HttpHandler accountHttpHandler = httpHandlerFactory.getAccountHttpHandler();
        RoutingHandler routingHandler = Handlers.routing()
                .post(TRANSFER_PATH, httpHandlerFactory.getTransferHttpHandler())
                .post(ACCOUNT_PATH, accountHttpHandler)
                .get(ACCOUNT_PATH, accountHttpHandler)
                .setFallbackHandler(httpHandlerFactory.get404HttpHandler());


        server = Undertow.builder()
                .addHttpListener(serverProperties.getPort(), serverProperties.getHost())
                .setHandler(routingHandler)
                .build();
        server.start();
    }

    public void stop() {
        if (server != null) {
            server.stop();
        }
    }

}
