package org.luk.transfer.server;

import org.luk.transfer.exceptions.NoAccountException;
import org.luk.transfer.exceptions.NotEnoughMoneyTransferException;
import org.luk.transfer.operation.TransferOperation;
import org.luk.transfer.util.PropertiesUtil;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.Deque;
import java.util.Map;

import static org.luk.transfer.util.HttpRequestUtil.*;

public class TransferHttpHandler implements HttpHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(PropertiesUtil.class);

    private TransferOperation transferOperation;

    TransferHttpHandler(TransferOperation transferOperation) {
        this.transferOperation = transferOperation;
    }

    @Override
    public void handleRequest(HttpServerExchange httpServerExchange) {
        try {
            doHandle(httpServerExchange);
        } catch (NoAccountException e) {
            LOGGER.error("No account.", e);
            httpServerExchange.setStatusCode(404)
                    .getResponseSender()
                    .send("Problem with handling request.");
        } catch (NotEnoughMoneyTransferException e) {
            LOGGER.error("Not enough money.", e);
            httpServerExchange.setStatusCode(400)
                    .getResponseSender()
                    .send("Not enough money on account");
        } catch (Throwable throwable) {
            LOGGER.error("Problem with handling request.", throwable);
            httpServerExchange.setStatusCode(500)
                    .getResponseSender()
                    .send("No account for a given id.");
        }

        httpServerExchange
                .setStatusCode(200)
                .getResponseSender().send("Transferred");
    }

    private void doHandle(HttpServerExchange httpServerExchange) {
        Map<String, Deque<String>> queryParameters = httpServerExchange.getQueryParameters();
        BigDecimal amount = getAmountParam(queryParameters);
        int fromId = getFromAccountParam(queryParameters);
        int toId = getToAccountParam(queryParameters);
        transferOperation.transfer(amount, fromId, toId);
    }

}
