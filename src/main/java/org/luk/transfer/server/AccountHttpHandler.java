package org.luk.transfer.server;

import com.google.gson.Gson;
import org.luk.transfer.account.Account;
import org.luk.transfer.exceptions.UniqueAccountRepositoryEception;
import org.luk.transfer.operation.AccountOperation;
import org.luk.transfer.util.HttpRequestUtil;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.Headers;
import io.undertow.util.HttpString;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.Deque;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static io.undertow.util.Methods.GET;
import static io.undertow.util.Methods.POST;

public class AccountHttpHandler implements HttpHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(AccountHttpHandler.class);

    private final AccountOperation accountOperation;


    public AccountHttpHandler(AccountOperation accountOperation) {
        this.accountOperation = accountOperation;
    }


    @Override
    public void handleRequest(HttpServerExchange httpServerExchange) {
        try {
            doHandle(httpServerExchange);
        } catch(UniqueAccountRepositoryEception e) {
            LOGGER.error("Not unique account.", e);
            httpServerExchange.setStatusCode(400)
                    .getResponseSender()
                    .send("Not unique account.");
        } catch (Throwable throwable) {
            LOGGER.error("Problem with handling request.", throwable);
            httpServerExchange.setStatusCode(500)
                    .getResponseSender()
                    .send("Problem with handling request.");
        }
    }

    private void doHandle(HttpServerExchange httpServerExchange) {
        Map<String, Deque<String>> queryParameters = httpServerExchange.getQueryParameters();

        HttpString requestMethod = httpServerExchange.getRequestMethod();

        if (POST.equals(requestMethod)) {
            createNewAccount(httpServerExchange, queryParameters);
        } else if (GET.equals(requestMethod)) {
            getAccountByID(httpServerExchange, queryParameters);
        }
    }

    private void getAccountByID(HttpServerExchange httpServerExchange, Map<String, Deque<String>> queryParameters) {
        int id = HttpRequestUtil.getIdParam(queryParameters);
        Optional<Account> account = accountOperation.getAccount(id);
        if (account.isPresent()) {
            Gson gson = new Gson();
            Map<String, String> result = new HashMap<>();
            result.put("id", String.valueOf(account.get().getId()));
            result.put("value", String.valueOf(account.get().getValue().doubleValue()));
            httpServerExchange.getResponseHeaders()
                    .put(Headers.CONTENT_TYPE, "application/json");
            httpServerExchange.setStatusCode(200)
                    .getResponseSender()
                    .send(gson.toJson(result));
        } else {
            httpServerExchange.setStatusCode(404)
                    .getResponseSender()
                    .close();
        }
    }

    private void createNewAccount(HttpServerExchange httpServerExchange, Map<String, Deque<String>> queryParameters) {
        int id = HttpRequestUtil.getIdParam(queryParameters);
        BigDecimal amount = HttpRequestUtil.getAmountParam(queryParameters);
        accountOperation.addAccount(id, amount);
        httpServerExchange.setStatusCode(201)
                .getResponseSender()
                .send("Account created");
    }
}
