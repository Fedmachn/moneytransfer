package org.luk.transfer.server;

import org.luk.transfer.operation.AccountOperation;
import org.luk.transfer.operation.TransferOperation;
import io.undertow.server.HttpHandler;

public class DefaultHttpHandlerFactory implements HttpHandlerFactory {

    private TransferOperation transferOperation;
    private AccountOperation accountOperation;

    public DefaultHttpHandlerFactory(TransferOperation transferOperation, AccountOperation accountOperation) {
        this.transferOperation = transferOperation;
        this.accountOperation = accountOperation;
    }

    @Override
    public HttpHandler getTransferHttpHandler() {
        return new TransferHttpHandler(transferOperation);
    }

    @Override
    public HttpHandler getAccountHttpHandler() {
        return new AccountHttpHandler(accountOperation);
    }

    @Override
    public HttpHandler get404HttpHandler() {
        return httpServerExchange -> httpServerExchange
                .setStatusCode(404)
                .getResponseSender()
                .send("It's not supported");
    }
}
