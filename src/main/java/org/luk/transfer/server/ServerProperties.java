package org.luk.transfer.server;

public interface ServerProperties {

    int getPort();

    String getHost();
}
