package org.luk.transfer.server;


import org.luk.transfer.util.PropertiesUtil;

public class ServerFileProperties implements ServerProperties {
    private static final String PREFIX = "server.";
    private static final String PORT = "port";
    private static final String HOST = "host";
    private static final String DEFAULT_PORT = "8080";
    private static final String DEFAULT_HOST = "localhost";

    @Override
    public int getPort() {
        return Integer.valueOf(PropertiesUtil.getOrDefaultProperty(PREFIX + PORT, DEFAULT_PORT));
    }

    @Override
    public String getHost() {
        return PropertiesUtil.getOrDefaultProperty(PREFIX + HOST, DEFAULT_HOST);
    }
}
